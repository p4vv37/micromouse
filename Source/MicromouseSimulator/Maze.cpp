// Fill out your copyright notice in the Description page of Project Settings.


#include "Maze.h"
#define NORTH         (uint8_t)0x01
#define EAST          (uint8_t)0x02
#define SOUTH         (uint8_t)0x04
#define WEST          (uint8_t)0x08


// Sets default values
AMaze::AMaze()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // maze anmouse parameters: http://robogames.net/rules/maze.php
    double elementWidth = ELEMENT_WIDTH;
    UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
    RootComponent = MeshComponent;
    FString meshName;

    for (int i=0; i<NUM_ROWS*(NUM_CELLS*2); i++)
    {
        meshName = "wall" + FString::FromInt(i);
        // Create and position a mesh component so we can see where our sphere is
        UStaticMeshComponent* MeshVisual = CreateDefaultSubobject<UStaticMeshComponent>(FName(*meshName));
        MeshVisual->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        MeshVisual->SetupAttachment(RootComponent);
        MeshVisual->SetMobility(EComponentMobility::Movable);

        FVector Position;
        if (i < NUM_CELLS*NUM_ROWS)
        {

            int column = i/(NUM_CELLS);
            int row = i%NUM_CELLS;

            Position.X = -row * elementWidth;
            Position.Y = column * elementWidth;
            Position.Z = 0.0f;
//            UE_LOG(LogTemp,Warning,TEXT("x, y: %d %d %f %f"), column, row, Position.X, Position.Y);

            MeshVisual->SetWorldLocation(Position + FVector(0, elementWidth/2.0, 0));
        }
        else
        {
            int column = (i - NUM_ROWS*NUM_CELLS)/16;
            int row = (i - NUM_ROWS*NUM_CELLS)%16;

            Position.X = -row * elementWidth;
            Position.Y = column * elementWidth;
            Position.Z = 0.0f;

            MeshVisual->SetWorldLocation(Position + FVector(-elementWidth/2.0, 0, 0));
            MeshVisual->SetRelativeRotation(FQuat(FVector(0, 0, 1), 3.14*1.5));
        }

        m_walls.Add(MeshVisual);
    }

    loadMaze(TEXT("1stworld.maz"));
}

// Called when the game starts or when spawned
void AMaze::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMaze::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMaze::loadMaze(const FString mazFile)
{
    FString RelativePath = FPaths::GameContentDir();
    RelativePath += "maz_files/";
    FString path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*RelativePath);
//    FString path = TEXT("/home/pawel/git/micromouse_maze_tool/mazefiles/binary/");
    path += mazFile;
    FILE *infile = fopen(TCHAR_TO_ANSI(*path), "r");
    const int MAZE_WIDTH = 16;
    const int MAZE_CELLS = MAZE_WIDTH * MAZE_WIDTH;

    uint8_t maze[MAZE_SIZE*MAZE_SIZE];

    fread(maze, 1, MAZE_CELLS, infile);

    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshVisualAsset(TEXT("/Game/meshes/wall.wall"));

    for (auto wall : m_walls)
    {
        wall->SetStaticMesh(m_emptyMesh);
    }

    for (int i=0; i<MAZE_SIZE*MAZE_SIZE; i++)
    {
        int column = i/16;
        int row = i%16;
        if (WEST & maze[i])
        {
            UStaticMeshComponent* wall = m_walls[row*NUM_CELLS + column];
            if (MeshVisualAsset.Succeeded())
            {
                wall->SetStaticMesh(MeshVisualAsset.Object);
            }
        }

        if (SOUTH & maze[i])
        {
            UStaticMeshComponent* wall = m_walls[NUM_ROWS*NUM_CELLS + row*16 + column];
            if (MeshVisualAsset.Succeeded())
            {
                wall->SetStaticMesh(MeshVisualAsset.Object);
            }
        }
    }

    for (int i=0; i<NUM_ROWS; i++)
    {

        UStaticMeshComponent* wallWest = m_walls[i*NUM_CELLS + 16];
        UStaticMeshComponent* wallNorth = m_walls[NUM_ROWS*NUM_CELLS + 16*16 + i];
        if (MeshVisualAsset.Succeeded())
        {
            wallWest->SetStaticMesh(MeshVisualAsset.Object);
            wallNorth->SetStaticMesh(MeshVisualAsset.Object);
        }
    }
}


