// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ConstructorHelpers.h"
#include "ProceduralMeshComponent.h"
#include "Maze.generated.h"

#define NUM_CELLS           17
#define NUM_ROWS            16

#define MAZE_SIZE           16

#define ELEMENT_WIDTH       180

UCLASS()
class MICROMOUSESIMULATOR_API AMaze : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AMaze();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    UFUNCTION(BlueprintCallable, Category = Default)
	void loadMaze(const FString mazFile);

private:
    char* m_maeMap;
    UPROPERTY(VisibleAnywhere)
    UProceduralMeshComponent * mesh;
    TArray<UStaticMeshComponent*> m_walls;
    UStaticMesh* m_emptyMesh;

};
