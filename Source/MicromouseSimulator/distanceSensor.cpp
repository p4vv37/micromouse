// Fill out your copyright notice in the Description page of Project Settings.


#include "distanceSensor.h"


// Sets default values
ADistanceSensor::ADistanceSensor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


    UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
    RootComponent = MeshComponent;

    // Create and position a mesh component so we can see where our sphere is
    UStaticMeshComponent* MeshVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
    MeshVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshVisualAsset(TEXT("StaticMesh'/Game/meshes/basic_sensor_Cube.basic_sensor_Cube'"));
    if (MeshVisualAsset.Succeeded())
    {
        MeshVisual->SetStaticMesh(MeshVisualAsset.Object);
    }

}

// Called when the game starts or when spawned
void ADistanceSensor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADistanceSensor::Tick(float DeltaTime)
{
    FCollisionQueryParams traceParams(FName(TEXT("")), false, GetOwner());
    FHitResult HitResult;
    auto hitVectorStart = GetActorLocation() + GetActorRotation().RotateVector(FVector(15, 0, 0));
    auto hitVectorEnd  = GetActorLocation() + GetActorRotation().RotateVector(FVector(STEP_SIZE/2 - 15, 0, 0));
    GetWorld()->LineTraceSingleByObjectType(
            OUT HitResult,
            hitVectorStart,
            hitVectorEnd,
            FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldDynamic),
            traceParams
    );
    float green_color = 0;
    AActor* ActorHit = HitResult.GetActor();
    if(ActorHit)
    {
        m_value = true;
//        UE_LOG(LogTemp, Warning, TEXT("!!!!!!!!!!!!!!!!!!!!!!! %s"), *(ActorHit->GetName()))
        green_color = 128;
    } else {
        m_value = false;
    }
	Super::Tick(DeltaTime);
    DrawDebugLine(
            GetWorld(),
            hitVectorStart,
            hitVectorEnd,
            FColor(128, green_color, 64),
            false,
            0,
            0,
            8
    );
}

float read()
{
    return 0.0f;
}
