// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "stepperMotor.h"
#include "distanceSensor.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Classes/Components/SphereComponent.h"
#include "RobotMovementComponent.h"
#include "Robot.generated.h"

#define STEP_SIZE 180
#define MAZE_SIZE           16

UENUM(BlueprintType)
enum class Direction : uint8
{
    Up 	    UMETA(DisplayName="Up"),
    Right 	UMETA(DisplayName="Right"),
    Down 	UMETA(DisplayName="Down"),
    Left 	UMETA(DisplayName="Left"),
};


USTRUCT(BlueprintType)
struct FMazeCell
{
    GENERATED_USTRUCT_BODY()
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
    bool left{false};
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
    bool top{false};
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
    bool right{false};
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
    bool bottom{false};

    bool closed{false};
    bool visited{false};
};

USTRUCT(BlueprintType)
struct FMazeMap
{
    GENERATED_USTRUCT_BODY()
    FMazeMap()
    {
        m_cellsArray.Init(FMazeCell(), MAZE_SIZE*MAZE_SIZE);
    }
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
        TArray<FMazeCell> m_cellsArray;

    FMazeCell* GetCell(const int& x, const int& y);
    void setCell(const int& x, const int& y, FMazeCell cell);
    void setCellWalls(const int& x, const int& y, bool top, bool right, bool bottom, bool left);
    void print(const int& pos_x, const int& pos_y);

    void setVisited(const int& pos_x, const int& pos_y);
    void setClosed(const int& pos_x, const int& pos_y);
    
};

UCLASS()
class MICROMOUSESIMULATOR_API ARobot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARobot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float WDeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void scanMaze();
	void goToStart();
	void solveMaze();
	void rotate(const double angle);
	void goForward(const double distance);
	void goBackward(const double distance);

    class URobotMovementComponent* m_robotMovementComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sensors)
	bool m_leftDistanceensor{false};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sensors)
	bool m_rightDistanceensor{false};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sensors)
	bool m_frontDistanceensor{false};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Default)
    float m_rotationSpeed{90};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Default)
    float m_speed{200};
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Sensors)
    FMazeMap m_mazeMap;

private:
    double m_posX{0};
    double m_posY{0};
    double m_angle{0};
    double m_distanceLeft{0};
//	AStepperMotor m_leftStepperMotor;
//	AStepperMotor m_rightStepperMotor;
	void collectSample();

	bool m_isProcessingClosed{true};

    Direction getDirection();
};
