// Fill out your copyright notice in the Description page of Project Settings.


#include "Robot.h"


FMazeCell *FMazeMap::GetCell(const int &x, const int &y) {
    return &m_cellsArray[MAZE_SIZE * x + y];
}

void FMazeMap::setCell(const int &x, const int &y, FMazeCell cell) {
    m_cellsArray[MAZE_SIZE * x + y] = cell;
}

void FMazeMap::setVisited(const int &pos_x, const int &pos_y)
{
m_cellsArray[MAZE_SIZE*pos_x + pos_y].visited = true;
}

void FMazeMap::setClosed(const int &pos_x, const int &pos_y)
{
m_cellsArray[MAZE_SIZE*pos_x + pos_y].closed = true;
}

void FMazeMap::setCellWalls(const int &x, const int &y, bool top, bool right, bool bottom, bool left) {
    if (top)
        m_cellsArray[MAZE_SIZE * x + y].top = true;
    if (right)
        m_cellsArray[MAZE_SIZE * x + y].right = true;
    if (bottom)
        m_cellsArray[MAZE_SIZE * x + y].bottom = true;
    if (left)
        m_cellsArray[MAZE_SIZE * x + y].left = true;
}

void FMazeMap::print(const int &pos_x, const int &pos_y) {
    FString out;
    out += TEXT("\n");
    for (int y = 15; y >= 0; y--) {
        FString top;
        FString middle;
        FString bottom;
        for (int x = 0; x < 16; x++) {
            FMazeCell *cell = this->GetCell(x, y);

            if (cell->left) {
                top += TEXT("X");
                middle += TEXT("X");
                bottom += TEXT("X");
            } else {
                if (cell->top) {
                    top += TEXT("X");
                } else {
                    top += TEXT("-");
                }

                middle += TEXT("-");

                if (cell->bottom) {
                    bottom += TEXT("X");
                } else {
                    bottom += TEXT("-");
                }
            }

            if (cell->top) {
                top += TEXT("X");
            } else {
                top += TEXT("-");
            }

            if (x == pos_x && y == pos_y) {
                middle += TEXT("O");
            } else {
                middle += TEXT(" ");
            }

            if (cell->bottom) {
                bottom += TEXT("X");
            } else {
                bottom += TEXT("-");
            }

            if (cell->right) {
                top += TEXT("X");
                middle += TEXT("X");
                bottom += TEXT("X");
            } else {
                if (cell->top) {
                    top += TEXT("X");
                } else {
                    top += TEXT("-");
                }

                middle += TEXT("-");

                if (cell->bottom) {
                    bottom += TEXT("X");
                } else {
                    bottom += TEXT("-");
                }
            }
        }
        out += top + TEXT("\n") + middle + TEXT("\n") + bottom + TEXT("\n");
    }
    UE_LOG(LogTemp, Warning, TEXT("%s"), *out);
}


// Sets default values
ARobot::ARobot() {
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;


    USphereComponent *SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
    RootComponent = SphereComponent;
    SphereComponent->InitSphereRadius(40.0f);
    SphereComponent->SetCollisionProfileName(TEXT("Pawn"));

    // Create and position a mesh component so we can see where our sphere is
    UStaticMeshComponent *SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
    SphereVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder <UStaticMesh> SphereVisualAsset(
            TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
    if (SphereVisualAsset.Succeeded()) {
        SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
    }

    // Create an instance of our movement component, and tell it to update the root.
    m_robotMovementComponent = CreateDefaultSubobject<URobotMovementComponent>(TEXT("CustomMovementComponent"));
    m_robotMovementComponent->UpdatedComponent = RootComponent;

}

// Called when the game starts or when spawned
void ARobot::BeginPlay() {
    m_mazeMap.setCellWalls(0, 0, false, false, true, false);
    Super::BeginPlay();

}

// Called every frame
void ARobot::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

    double angleToRotate = m_rotationSpeed * DeltaTime;
    if (m_angle < 0)
        angleToRotate = -angleToRotate;

    if (abs(angleToRotate) > abs(m_angle)) {
        angleToRotate = m_angle;
        m_angle = 0.0f;
    } else {
        m_angle -= angleToRotate;
    }
//    UE_LOG(LogTemp, Warning, TEXT("distance: %f, %f angle: %f, %f"), m_distanceLeft, m_distanceLeft, angleToRotate, m_angle);

    if (abs(angleToRotate) > 0.001) {
        rotate(angleToRotate);
        return;
    }

    if (abs(STEP_SIZE - m_distanceLeft) < 0.01)
        collectSample();

    if ((m_posY == 7 || m_posY == 8) && (m_posX == 7 || m_posX == 8))
        return;

    double distanceToMove = m_speed * DeltaTime;
    if (distanceToMove > m_distanceLeft) {
        distanceToMove = m_distanceLeft;
        m_distanceLeft = 0.0f;
    } else {
        m_distanceLeft -= distanceToMove;
    }

    if (distanceToMove > 0.001) {
//        AddActorLocalOffset(distanceToMove * GetActorForwardVector(), true);
        goForward(distanceToMove);
        return;
    }

    if (m_distanceLeft < 0.001 && abs(m_angle) < 0.001) {

        m_distanceLeft = STEP_SIZE;
        int opened_walls = !m_frontDistanceensor + !m_leftDistanceensor + !m_rightDistanceensor;
        UE_LOG(LogTemp, Warning, TEXT("openedWalls %d, %d, %d: %d"), !m_leftDistanceensor, !m_frontDistanceensor,
               !m_rightDistanceensor, opened_walls);
        if (opened_walls > 0) {
            if (opened_walls > 1) {
                m_isProcessingClosed = false;
            } else {
                if (m_isProcessingClosed) {
                    m_mazeMap.setClosed(m_posX, m_posY);
                }
            }

            Direction direction = getDirection();
            TArray<int> options;
            int decisioon = FMath::RandRange(0, opened_walls - 1);

            FMazeCell *cellUp;
            if (m_posY + 1 < 16)
                cellUp = m_mazeMap.GetCell(m_posX, m_posY + 1);
            FMazeCell *cellRight;
            if (m_posX + 1 < 16)
                cellRight = m_mazeMap.GetCell(m_posX + 1, m_posY);
            FMazeCell *cellDown;
            if (m_posY - 1 >- 0)
                cellDown = m_mazeMap.GetCell(m_posX, m_posY - 1);
            FMazeCell *cellLeft;
            if (m_posX - 1 >= 0)
                cellLeft = m_mazeMap.GetCell(m_posX - 1, m_posY);

            if (!m_leftDistanceensor) {
                if ((cellLeft && (direction == Direction::Up && !cellLeft->closed)) ||
                    (cellUp &&(direction == Direction::Right && !cellUp->closed)) ||
                    (cellRight && (direction == Direction::Down && !cellRight->closed)) ||
                    (cellDown && (direction == Direction::Left && !cellDown->closed)))
                {
                    options.Add(-90);
                }
            }
            if (!m_frontDistanceensor) {
                if ((cellUp && (direction == Direction::Up && !cellUp->closed)) ||
                    (cellRight && (direction == Direction::Right && !cellRight->closed)) ||
                    (cellDown && (direction == Direction::Down && !cellDown->closed)) ||
                    (cellLeft && (direction == Direction::Left && !cellLeft->closed)))
                {
                    options.Add(0);
                }
            }
            if (!m_rightDistanceensor) {
                if ((cellRight && (direction == Direction::Up && !cellRight->closed)) ||
                    (cellDown && (direction == Direction::Right && !cellDown->closed)) ||
                    (cellLeft && (direction == Direction::Down && !cellLeft->closed)) ||
                    (cellUp && (direction == Direction::Left && !cellUp->closed)))
                {
                    options.Add(90);
                }
            }

            m_angle = options[decisioon];
        } else {
            m_angle = 180;
            m_isProcessingClosed = true;
            m_mazeMap.setClosed(m_posX, m_posY);
        }
        if (abs(m_angle) > 0.01)
            collectSample();
//        UE_LOG(LogTemp, Warning, TEXT("Angle %f"), m_angle);
    }
}


// Called to bind functionality to input
void ARobot::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent) {
    Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ARobot::solveMaze() {

}

void ARobot::scanMaze() {

}

void ARobot::goToStart() {

}

void ARobot::goBackward(const double distance) {

}

void ARobot::goForward(const double distance) {
    FVector loc = GetActorLocation();
    FVector forward = GetActorForwardVector();
    FVector newLocation = loc + (forward * distance);
    SetActorLocation(newLocation);
}

Direction ARobot::getDirection() {
    FRotator rot = GetActorRotation();
    rot.Clamp();

    if ((-45 < rot.Yaw && rot.Yaw < 45) || rot.Yaw > 315) {
        return Direction::Up;
    } else if ((45 < rot.Yaw && rot.Yaw < 125) || -315 > rot.Yaw) {
        return Direction::Right;
    } else if ((125 < rot.Yaw && rot.Yaw < 215) || (-125 > rot.Yaw && rot.Yaw > -215)) {
        return Direction::Down;
    } else {
        return Direction::Left;
    }


}

void ARobot::rotate(const double angle) {
    FRotator rot = GetActorRotation();
    rot.Yaw += angle;
    SetActorRotation(rot);
}

void ARobot::collectSample() {
    m_mazeMap.setVisited(m_posX, m_posY);
    FRotator rot = GetActorRotation();
    rot.Clamp();
    UE_LOG(LogTemp, Warning, TEXT("%f, %d, %d"), rot.Yaw, -45 < rot.Yaw && rot.Yaw < 45, rot.Yaw > 315);

    Direction direction = getDirection();
    FString direction_symbol;

    float posXBefore = m_posX;
    float posYBefore = m_posY;


    if (direction == Direction::Up) {
        m_mazeMap.setCellWalls(m_posX, m_posY, m_frontDistanceensor, m_rightDistanceensor, false, m_leftDistanceensor);
        if (abs(m_angle) < 0.01)
            m_posY += 1;
        direction_symbol = TEXT("^");
    } else if (direction == Direction::Right) {
        m_mazeMap.setCellWalls(m_posX, m_posY, m_leftDistanceensor, m_frontDistanceensor, m_rightDistanceensor, false);
        if (abs(m_angle) < 0.01)
            m_posX += 1;
        direction_symbol = TEXT(">");
    } else if (direction == Direction::Down) {
        m_mazeMap.setCellWalls(m_posX, m_posY, false, m_leftDistanceensor, m_frontDistanceensor, m_rightDistanceensor);
        if (abs(m_angle) < 0.01)
            m_posY -= 1;
        direction_symbol = TEXT("v");
    } else {
        m_mazeMap.setCellWalls(m_posX, m_posY, m_rightDistanceensor, false, m_leftDistanceensor, m_frontDistanceensor);
        if (abs(m_angle) < 0.01)
            m_posX -= 1;
        direction_symbol = TEXT("<");
    }

    m_mazeMap.print(m_posX, m_posY);

    UE_LOG(LogTemp, Warning, TEXT("%s X, Y: %f, %f"), *direction_symbol, posXBefore, posYBefore);

}

