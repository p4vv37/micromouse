// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "stepperMotor.generated.h"

UCLASS()
class MICROMOUSESIMULATOR_API AStepperMotor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStepperMotor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void rotate(const double angle);
	bool isWorking(){ return m_isWorking; };

private:
    bool m_isWorking{false};
    double m_currentAngle{0};
    double m_targetAngle{0};

	
	
};
