// Fill out your copyright notice in the Description page of Project Settings.


#include "stepperMotor.h"


// Sets default values
AStepperMotor::AStepperMotor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


    UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
    RootComponent = MeshComponent;

    // Create and position a mesh component so we can see where our sphere is
    UStaticMeshComponent* MeshVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
    MeshVisual->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshVisualAsset(TEXT("StaticMesh'/Game/meshes/wheel_basic.wheel_basic'"));
    if (MeshVisualAsset.Succeeded())
    {
        MeshVisual->SetStaticMesh(MeshVisualAsset.Object);
    }

}

// Called when the game starts or when spawned
void AStepperMotor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStepperMotor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStepperMotor::rotate(const double angle) {

}

